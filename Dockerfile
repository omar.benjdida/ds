FROM openjdk:7-jre-slim
WORKDIR /target
COPY ./target/ .
COPY run.sh /target

RUN chmod 777 run.sh
RUN mvn --batch-mode -f /usr/src/app/pom.properties clean package.

FROM openjdk:7-jre-slim
EXPOSE 8080

CMD ["sh","run.sh"]